package com.example.poko.apiservices;

import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

/**
 * Created by Poko on 05-07-2016.
 */
public class ListAdapter extends RecyclerView.Adapter<NameListViewHolder> {

    public List<ContactList> adapterOfList;

    public ListAdapter(List<ContactList> contactLists) {
        this.adapterOfList = contactLists;

    }

    @Override
    public NameListViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View view = inflater.inflate(R.layout.list_row, null);
        return new NameListViewHolder(view);
    }

    @Override
    public void onBindViewHolder(NameListViewHolder holder, int position) {
        holder.mList = this.adapterOfList.get(position);
        holder.bind();


    }

    @Override
    public int getItemCount() {
        return adapterOfList.size();
    }
}
