package com.example.poko.apiservices;

import android.util.Log;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Poko on 05-07-2016.
 */
public class SubscriberList {
    @SerializedName("id")
    @Expose
    private String sub_id;

    @SerializedName("email")
    @Expose
    private String sub_email;

    public String getsub_Id() {

        return sub_id;
    }


    public void setsub_Id(String id) {
        this.sub_id = id;
    }


    public String gesub_temail() {
       // Log.d("CodeKamp","Call 5");

        return sub_email;
    }


    public void setsub_Name(String name) {
        this.sub_email = name;
    }



}



