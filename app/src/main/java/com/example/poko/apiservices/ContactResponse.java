package com.example.poko.apiservices;

import android.util.Log;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Poko on 05-07-2016.
 */
public class ContactResponse {

    @SerializedName("data")
    @Expose
    private List<SubscriberList> mSubscriberLists = new ArrayList<SubscriberList>();


    public List<SubscriberList> getSubscriberLists() {
       // Log.d("CodeKamp","Call 4");

        return mSubscriberLists;
    }

    /**
     * @param data The data
     */
    public void setContactLists(List<SubscriberList> data) {
        this.mSubscriberLists = data;
    }

}

