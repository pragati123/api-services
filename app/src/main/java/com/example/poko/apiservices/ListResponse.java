
package com.example.poko.apiservices;

import android.util.Log;

import java.util.ArrayList;
import java.util.List;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ListResponse {


    @SerializedName("data")
    @Expose
    private List<ContactList> mContactLists = new ArrayList<ContactList>();


    public List<ContactList> getContactLists() {
        return mContactLists;
    }

    /**
     * @param data The data
     */
    public void setContactLists(List<ContactList> data) {
        this.mContactLists = data;
    }

}
