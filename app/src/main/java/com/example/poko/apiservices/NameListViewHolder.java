package com.example.poko.apiservices;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

/**
 * Created by Poko on 05-07-2016.
 */
public class NameListViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
    public  ContactList mList;
    private TextView label;
    public Context holder_context;


    public NameListViewHolder(View itemView) {
        super(itemView);

        label=(TextView)itemView.findViewById(R.id.list_textlabel);
        holder_context=itemView.getContext();
        itemView.setOnClickListener(this);
    }

    public void bind(){
        this.label.setText(mList.getName());
    }

    @Override
    public void onClick(View v) {
        Intent intent= SubscriberActivity.newIntent(holder_context, this.mList.getId().toString());
        Log.d("CodeKamp",this.mList.getId());
        holder_context.startActivity(intent);
    }
}
