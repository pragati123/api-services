package com.example.poko.apiservices;

import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

/**
 * Created by Poko on 05-07-2016.
 */
public class SubscriberAdapter extends RecyclerView.Adapter<SubscriberViewHolder> {

    public List<SubscriberList> adapterOfSubscriber;

    public SubscriberAdapter(List<SubscriberList> sbuList){
        this.adapterOfSubscriber=sbuList;
    }

    @Override
    public SubscriberViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View view = inflater.inflate(R.layout.subscriber_row, null);
       // Log.d("CodeKamp","Call 6");

        return new SubscriberViewHolder(view);
    }

    @Override
    public void onBindViewHolder(SubscriberViewHolder holder, int position) {
        holder.mSubList=this.adapterOfSubscriber.get(position);
        holder.subBind();
       // Log.d("CodeKamp","Call 8");


    }

    @Override
    public int getItemCount() {
       // Log.d("CodeKamp","Call 9");
       // Log.d("CodeKamp",Integer.toString( adapterOfSubscriber.size()));

        return adapterOfSubscriber.size();
    }
}
