package com.example.poko.apiservices;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;


public class SubscriberActivity extends AppCompatActivity {

    private RecyclerView sub_recycle;
    public static  final String list_id="listId";
    public List<SubscriberList> mSubscriberLists;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
       // Log.d("CodeKamp","Call 1");
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_subscriber);

        sub_recycle=(RecyclerView)findViewById(R.id.Subscriber_recyclerview);

        Intent intent=getIntent();
      //  Log.d("CodeKamp","Call 2");

        String recieved_list_id=intent.getStringExtra(list_id);
        Log.d("CodeKamp",recieved_list_id);

        sub_recycle.setLayoutManager(new LinearLayoutManager(this));

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("https://us13.api.mailchimp.com/2.0/")
                .addConverterFactory(GsonConverterFactory.create())
                .build();



        MailChimpServices mailchimpService = retrofit.create(MailChimpServices.class);

        Call<ContactResponse> responseCall= mailchimpService.fetchContacts("bb29d631b9f9f64ef5338869b81a74a1-us13",recieved_list_id);
       // Log.d("CodeKamp","Call 3");


        responseCall.enqueue(new Callback<ContactResponse>() {
            @Override
            public void onResponse(Call<ContactResponse> call, Response<ContactResponse> response) {
               /* Log.d("CodeKamp",response.body().toString());
                for (int i=0; i<response.body().getSubscriberLists().size();i++){
                    Log.d("CodeKamp",response.body().getSubscriberLists().get(i).gesub_temail());
                }*/
               mSubscriberLists=response.body().getSubscriberLists();

                //Log.d("CodeKamp",response.body().getSubscriberLists().get(0).gesub_temail());

                SubscriberAdapter sub_adapter= new SubscriberAdapter(mSubscriberLists);
                sub_recycle.setAdapter(sub_adapter);
               /* for (int i=0; i<response.body().getSubscriberLists().size();i++){
                    Log.d("CodeKamp",response.body().getSubscriberLists().get(i).gesub_temail());
                }*/

            }

            @Override
            public void onFailure(Call<ContactResponse> call, Throwable t) {

                Log.d("CodeKamp", t.getMessage());
                t.printStackTrace();
            }
        });


    }

    public static Intent newIntent(Context context, String id){
        Intent intent=new Intent(context,SubscriberActivity.class);
        intent.putExtra(list_id,id);
        return intent;
    }
}
