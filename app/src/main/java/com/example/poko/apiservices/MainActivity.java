package com.example.poko.apiservices;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class MainActivity extends AppCompatActivity  {

    public List<ContactList> mLists;
    private RecyclerView list_rescycleview;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);


        setContentView(R.layout.activity_main);
        list_rescycleview=(RecyclerView)findViewById(R.id.recycler_lists);
        list_rescycleview.setLayoutManager(new LinearLayoutManager(this));


        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("https://us13.api.mailchimp.com/2.0/")
                .addConverterFactory(GsonConverterFactory.create())
                .build();





        MailChimpServices mailchimpService = retrofit.create(MailChimpServices.class);



        Call<ListResponse> call = mailchimpService.fetchLists("bb29d631b9f9f64ef5338869b81a74a1-us13");

        call.enqueue(new Callback<ListResponse>() {
            @Override
            public void onResponse(Call<ListResponse> call, Response<ListResponse> response) {
               /* for (int i=0; i<response.body().getContactLists().size();i++){
                    Log.d("CodeKamp",response.body().getContactLists().get(i).getName());
                }*/
               mLists=response.body().getContactLists();
               // Log.d("CodeKamp",mListResponses.get(0).getName());
                ListAdapter adapter=new ListAdapter(mLists);
                list_rescycleview.setAdapter(adapter);


            }

            @Override
            public void onFailure(Call<ListResponse> call, Throwable t) {
                Log.d("CodeKamp", t.getMessage());
                t.printStackTrace();
            }
        });

    }

}
