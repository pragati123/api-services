package com.example.poko.apiservices;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Query;

/**
 * Created by Poko on 05-07-2016.
 */
public interface MailChimpServices {

    @GET("lists/list")
    Call<ListResponse> fetchLists(@Query("apikey") String apiKey);

    @GET("lists/members")
    Call<ContactResponse> fetchContacts(@Query("apikey") String apiKey, @Query("id") String listId);

}
